package com.BryanCantos.cuartoB;

import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class fragmentos extends AppCompatActivity implements View.OnClickListener, frg_uno.OnFragmentInteractionListener, frg_dos.OnFragmentInteractionListener{

    Button botonFranUno, botonFranDos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmentos);

        botonFranUno = (Button) findViewById(R.id.btnFrgUno);
        botonFranDos = (Button)findViewById(R.id.btnFrgDos);

        botonFranUno.setOnClickListener(this);
        botonFranDos.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnFrgUno:
                frg_uno frgmentoUno = new frg_uno();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,frgmentoUno);
                transaction.commit();
                break;
        }
        switch (v.getId()){
            case R.id.btnFrgDos:
                frg_dos frgmentoDos = new frg_dos();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.contenedor,frgmentoDos);
                transaction.commit();
                break;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
